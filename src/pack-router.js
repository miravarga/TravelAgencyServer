import {
  OK, NOT_FOUND, LAST_MODIFIED, NOT_MODIFIED, BAD_REQUEST, ETAG,
  CONFLICT, METHOD_NOT_ALLOWED, NO_CONTENT, CREATED, FORBIDDEN, setIssueRes
} from './utils';
import Router from 'koa-router';
import {getLogger} from './utils';

const log = getLogger('pack');

let packsLastUpdateMillis = null;

export class PackRouter extends Router {
  constructor(props) {
    super(props);
    this.packStore = props.packStore;
    this.io = props.io;
    this.get('/', async(ctx) => {
      let res = ctx.response;
      let lastModified = ctx.request.get(LAST_MODIFIED);
      if (lastModified && packsLastUpdateMillis && packsLastUpdateMillis <= new Date(lastModified).getTime()) {
        log('search / - 304 Not Modified (the client can use the cached data)');
        res.status = NOT_MODIFIED;
      } else {
        res.body = await this.packStore.find({user: ctx.state.user._id});
        if (!packsLastUpdateMillis) {
          packsLastUpdateMillis = Date.now();
        }
        res.set({[LAST_MODIFIED]: new Date(packsLastUpdateMillis)});
        log('search / - 200 Ok');
      }
    }).get('/:id', async(ctx) => {
      let pack = await this.packStore.findOne({_id: ctx.params.id});
      let res = ctx.response;
      if (pack) {
        if (pack.user == ctx.state.user._id) {
          log('read /:id - 200 Ok');
          this.setPackRes(res, OK, pack); //200 Ok
        } else {
          log('read /:id - 403 Forbidden');
          setIssueRes(res, FORBIDDEN, [{error: "It's not your pack"}]);
        }
      } else {
        log('read /:id - 404 Not Found (if you know the resource was deleted, then you can return 410 Gone)');
        setIssueRes(res, NOT_FOUND, [{warning: 'Pack not found'}]);
      }
    }).post('/', async(ctx) => {
      let pack = ctx.request.body;
      let res = ctx.response;
      if (pack.text) { //validation
        pack.user = ctx.state.user._id;
        await this.createPack(ctx, res, pack);
      } else {
        log(`create / - 400 Bad Request`);
        setIssueRes(res, BAD_REQUEST, [{error: 'Text is missing'}]);
      }
    }).put('/:id', async(ctx) => {
      let pack = ctx.request.body;
      let id = ctx.params.id;
      let packId = pack._id;
      let res = ctx.response;
      if (packId && packId != id) {
        log(`update /:id - 400 Bad Request (param id and body _id should be the same)`);
        setIssueRes(res, BAD_REQUEST, [{error: 'Param id and body _id should be the same'}]);
        return;
      }
      if (!pack.text) {
        log(`update /:id - 400 Bad Request (validation errors)`);
        setIssueRes(res, BAD_REQUEST, [{error: 'Text is missing'}]);
        return;
      }
      if (!packId) {
        await this.createPack(ctx, res, pack);
      } else {
        let persistedPack = await this.packStore.findOne({_id: id});
        if (persistedPack) {
          if (persistedPack.user != ctx.state.user._id) {
            log('update /:id - 403 Forbidden');
            setIssueRes(res, FORBIDDEN, [{error: "It's not your pack"}]);
            return;
          }
          let packVersion = parseInt(ctx.request.get(ETAG)) || pack.version;
          if (!packVersion) {
            log(`update /:id - 400 Bad Request (no version specified)`);
            setIssueRes(res, BAD_REQUEST, [{error: 'No version specified'}]); //400 Bad Request
          } else if (packVersion < persistedPack.version) {
            log(`update /:id - 409 Conflict`);
            setIssueRes(res, CONFLICT, [{error: 'Version conflict'}]); //409 Conflict
          } else {
            pack.version = packVersion + 1;
            pack.updated = Date.now();
            let updatedCount = await this.packStore.update({_id: id}, pack);
            packsLastUpdateMillis = pack.updated;
            if (updatedCount == 1) {
              this.setPackRes(res, OK, pack); //200 Ok
              this.io.to(ctx.state.user.username).emit('pack/updated', pack);
            } else {
              log(`update /:id - 405 Method Not Allowed (resource no longer exists)`);
              setIssueRes(res, METHOD_NOT_ALLOWED, [{error: 'Pack no longer exists'}]); //
            }
          }
        } else {
          log(`update /:id - 405 Method Not Allowed (resource no longer exists)`);
          setIssueRes(res, METHOD_NOT_ALLOWED, [{error: 'Pack no longer exists'}]); //Method Not Allowed
        }
      }
    }).del('/:id', async(ctx) => {
      let id = ctx.params.id;
      await this.packStore.remove({_id: id, user: ctx.state.user._id});
      this.io.to(ctx.state.user.username).emit('pack/deleted', {_id: id})
      packsLastUpdateMillis = Date.now();
      ctx.response.status = NO_CONTENT;
      log(`remove /:id - 204 No content (even if the resource was already deleted), or 200 Ok`);
    });
  }

  async createPack(ctx, res, pack) {
    pack.version = 1;
    pack.updated = Date.now();
    let insertedPack = await this.packStore.insert(pack);
    packsLastUpdateMillis = pack.updated;
    this.setPackRes(res, CREATED, insertedPack); //201 Created
    this.io.to(ctx.state.user.username).emit('pack/created', insertedPack);
  }

  setPackRes(res, status, pack) {
    res.body = pack;
    res.set({
      [ETAG]: pack.version,
      [LAST_MODIFIED]: new Date(pack.updated)
    });
    res.status = status; //200 Ok or 201 Created
  }
}
