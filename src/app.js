import Koa from 'koa';
import cors from 'koa-cors';
import convert from 'koa-convert';
import bodyParser from 'koa-bodyparser';
import Router from 'koa-router';
import http from 'http';
import socketIo from 'socket.io';
import dataStore from 'nedb-promise';
import {getLogger, timingLogger, errorHandler, jwtConfig} from './utils';
import {PackRouter} from './pack-router';
import {AuthRouter} from './auth-router';
import koaJwt from 'koa-jwt';
import socketioJwt from 'socketio-jwt';

const app = new Koa();
const router = new Router();
const server = http.createServer(app.callback());
const io = socketIo(server);
const log = getLogger('app');

app.use(timingLogger);
app.use(errorHandler);

app.use(bodyParser());
app.use(convert(cors()));

const apiUrl = '/api';

log('config public routes');
const authApi = new Router({prefix: apiUrl})
const userStore = dataStore({filename: '../travelPackages.json', autoload: true});
authApi.use('/auth', new AuthRouter({userStore, io}).routes())
app.use(authApi.routes()).use(authApi.allowedMethods())

log('config protected routes');
app.use(convert(koaJwt(jwtConfig)));
const protectedApi = new Router({prefix: apiUrl})
const packStore = dataStore({filename: '../travelPackages' +
'.json', autoload: true});
protectedApi.use('/travelPackages', new PackRouter({packStore, io}).routes())
app.use(protectedApi.routes()).use(protectedApi.allowedMethods());

log('config socket io');
// io.on('connection', (socket) => {
//   log('client connected');
//   socket.on('disconnect', () => {
//     log('client disconnected');
//   })
// });
io.on('connection', socketioJwt.authorize(jwtConfig))
  .on('authenticated', (socket) => {
    const username = socket.decoded_token.username;
    socket.join(username);
    log(`${username} authenticated and joined`);
    socket.on('disconnect', () => {
      log('${username} disconnected');
    })
  });

(async() => {
  log('ensure default data');
  const ensureUserAndPacks = async(username) => {
    let user = await userStore.findOne({username: username});
    if (user) {
      log(`user ${username} was in the store`);
    } else {
      user = await userStore.insert({username, password: username});
      log(`user added ${JSON.stringify(user)}`);
    }
    let packs = await packStore.find({user: user._id});
    if (packs.length > 0) {
      log(`user ${username} had ${packs.length} packs`);
    } else {
      for (let i = 0; i < 3; i++) {
        let pack = await packStore.insert({
          text: `Pack ${username}${i}`,
          status: "active",
          updated: Date.now(),
          user: user._id,
          version: 1
        });
        log(`pack added ${JSON.stringify(pack)}`);
      }
    }
  };
  await Promise.all(['a', 'b'].map(username => ensureUserAndPacks(username)));
})();

server.listen(3000);